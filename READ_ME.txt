Instructions:

1 - copy the folder simple_ui to your computer
2 - open maya
3 - in the python console type:

# maya start up
path = r'C:\where\i_put\the_folder'  # <- path to where the tool folder was copied
import sys
sys.path.append(path)
from simple_ui import simple_dialog 
reload(simple_dialog)
ui = simple_dialog.main()

4 - execute by selecting the code and hitting ctrl + numpad enter