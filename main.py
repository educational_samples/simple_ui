# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\egrana\simple_ui\main.ui'
#
# Created: Mon Dec  2 11:42:49 2019
#      by: pyside2-uic  running on PySide2 2.0.0~alpha0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(467, 104)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.camera_comboBox = QtWidgets.QComboBox(self.groupBox_2)
        self.camera_comboBox.setObjectName("camera_comboBox")
        self.horizontalLayout_3.addWidget(self.camera_comboBox)
        self.renderable_checkBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.renderable_checkBox.setObjectName("renderable_checkBox")
        self.horizontalLayout_3.addWidget(self.renderable_checkBox)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Simple Example", None, -1))
        self.groupBox_2.setTitle(QtWidgets.QApplication.translate("Dialog", "Set Camera renderable", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("Dialog", "Camera", None, -1))
        self.renderable_checkBox.setText(QtWidgets.QApplication.translate("Dialog", "Set Renderable", None, -1))

