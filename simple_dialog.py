'''
Created on 26 nov. 2019

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is
import sys
sys.path.append(path)
from simple_ui import simple_dialog
reload(simple_dialog)  # <- to take changes that you made
ui = simple_dialog.main()  # need to store the dialog in the ui variable so it's not garbage collected

@author: eduardo
'''
import os
from maya import cmds

import loadUiType


# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'main.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, parent=None):

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        # populate the cameras
        self.populateCameras()
        # update this time by hand the status on the checkbox because we dont have the signals connected yet
        self.updateRenderableCheckbox()

        # connect the combo so if it changes the checkbox updates to the current status of the attribute on the
        # camera
        self.camera_comboBox.currentIndexChanged.connect(self.updateRenderableCheckbox)

        # connect the checkbox so when it is clicked it updates the value of the attribute on the camera
        self.renderable_checkBox.clicked.connect(self.toggleRenderable)

    def populateCameras(self):
        ''' add cameras in scene to combo box
        '''
        # clear the combo content
        self.camera_comboBox.clear()

        # list cameras, and if there is at leat one, add them to the combo
        cameras = cmds.ls(typ='camera')
        if cameras:
            self.camera_comboBox.addItems(cameras)

    def toggleRenderable(self):
        ''' changes the camera attribute to match the checkbox '''
        # get the camera name on the combo box
        camera = self.camera_comboBox.currentText()

        # get value form checkbox
        value = self.renderable_checkBox.isChecked()

        # set the value in the camera attribute
        cmds.setAttr(camera + '.renderable', value)

    def updateRenderableCheckbox(self):
        ''' changes the checkbox to match the camera attribute '''
        # get the camera name on the combo box
        camera = self.camera_comboBox.currentText()

        # get the current status of the attribute on the camera
        value = cmds.getAttr(camera + '.renderable')

        # we want to change the checkbox without triggering an udpate of the attribute on the camera
        # so we block the signals temporarily
        self.renderable_checkBox.blockSignals(True)

        # set the checkbox to mathc the current status of the camera attribute
        self.renderable_checkBox.setChecked(value)

        # we remove the block so it cna continue to emit signals
        self.renderable_checkBox.blockSignals(False)


def main():
    # create the ui and show it
    ui = MainUI()
    ui.show()

    # return it so it is not garbage collected
    return ui
