'''
Created on 26 nov. 2019

EXAMPLE CONVERTING UI TO PY INSTEAD OF DOING ALL IN RUNTIME

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is
import sys
sys.path.append(path)
from simple_ui import simple_dialog_from_py
reload(simple_dialog_from_py)  # <- to take changes that you made
ui = simple_dialog_from_py.main()  # need to store the dialog in the ui variable so it's not garbage collected

This is how to convert the .ui into .py

// open a dos shell and go to the maya bin folder, for example
cd C:\Program Files\Autodesk\Maya2018\bin

// run the "pyside2-uic" python script with the "mayapy" interpreter using as first argument the .ui file and
// as second argument the -o flag with the output module path
mayapy.exe pyside2-uic "C:\Users\egrana\simple_ui\main.ui" -o "C:\Users\egrana\simple_ui\main.py"



@author: eduardo
'''
from maya import cmds

from PySide2 import QtWidgets

import main  # the .ui converted .py


class MainUI(QtWidgets.QDialog, main.Ui_Dialog):  # the class inherits both form the Qt class and the UI class
    ''' This class implements the main ui '''

    def __init__(self, parent=None):

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        # populate the cameras
        self.populateCameras()
        # update this time by hand the status on the checkbox because we dont have the signals connected yet
        self.updateRenderableCheckbox()

        # connect the combo so if it changes the checkbox updates to the current status of the attribute on the
        # camera
        self.camera_comboBox.currentIndexChanged.connect(self.updateRenderableCheckbox)

        # connect the checkbox so when it is clicked it updates the value of the attribute on the camera
        self.renderable_checkBox.clicked.connect(self.toggleRenderable)

    def populateCameras(self):
        ''' add cameras in scene to combo box
        '''
        # clear the combo content
        self.camera_comboBox.clear()

        # list cameras, and if there is at leat one, add them to the combo
        cameras = cmds.ls(typ='camera')
        if cameras:
            self.camera_comboBox.addItems(cameras)

    def toggleRenderable(self):
        ''' changes the camera attribute to match the checkbox '''
        # get the camera name on the combo box
        camera = self.camera_comboBox.currentText()

        # get value form checkbox
        value = self.renderable_checkBox.isChecked()

        # set the value in the camera attribute
        cmds.setAttr(camera + '.renderable', value)

    def updateRenderableCheckbox(self):
        ''' changes the checkbox to match the camera attribute '''
        # get the camera name on the combo box
        camera = self.camera_comboBox.currentText()

        # get the current status of the attribute on the camera
        value = cmds.getAttr(camera + '.renderable')

        # we want to change the checkbox without triggering an udpate of the attribute on the camera
        # so we block the signals temporarily
        self.renderable_checkBox.blockSignals(True)

        # set the checkbox to mathc the current status of the camera attribute
        self.renderable_checkBox.setChecked(value)

        # we remove the block so it cna continue to emit signals
        self.renderable_checkBox.blockSignals(False)


def main():
    # create the ui and show it
    ui = MainUI()
    ui.show()

    # return it so it is not garbage collected
    return ui
